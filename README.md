# hello-mams

This project demonstrates how a MAMS agent can interact with a separate
Spring-Boot based REST service.

The deployed service is a greeting service that returns "Hello, MAMS"

The MAMS agent requests the greeting and displays it.

# Running the example

Go into the "greeting-service" folder and type:

`$ mvn spring-boot:run`

Go into the "mams-greeter" folder and type:

`$ mvn compile astra:deploy`

